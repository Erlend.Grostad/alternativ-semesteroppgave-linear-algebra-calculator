package org.openjfx.Calculator.Controller;

import org.junit.jupiter.api.Test;
import org.openjfx.Calculator.Model.GridPaneObject;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javafx.scene.layout.GridPane;

public class initialSceneControllerTest {
    
    @Test
    void addColumnTest() {
        GridPane gridpane = new GridPane();
        GridPane gridpane2 = new GridPane();
        initialSceneController controller = new initialSceneController();
        GridPaneObject gridpaneobject = new GridPaneObject(gridpane, 2, 2);
        GridPaneObject gridpaneobject2 = new GridPaneObject(gridpane2, 3, 2);


        controller.addColumn(gridpaneobject);
        assertEquals(gridpaneobject2.getDataAsArrayListDouble(), gridpaneobject.getDataAsArrayListDouble());
    }


    @Test
    void removeColumnTest() {
        GridPane gridpane = new GridPane();
        GridPane gridpane2 = new GridPane();
        initialSceneController controller = new initialSceneController();
        GridPaneObject gridpaneobject = new GridPaneObject(gridpane, 3, 3);
        GridPaneObject gridpaneobject2 = new GridPaneObject(gridpane2, 2, 3);

        controller.removeColumn(gridpaneobject);
        assertEquals(gridpaneobject2.getDataAsArrayListDouble(), gridpaneobject.getDataAsArrayListDouble());
    }
}
