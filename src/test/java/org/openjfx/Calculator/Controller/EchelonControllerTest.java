package org.openjfx.Calculator.Controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.openjfx.Calculator.Model.GridPaneObject;

import javafx.scene.layout.GridPane;

public class EchelonControllerTest {

    @Test
    void testReduceMatrix3x4() {

        //Testing 3x4 matrix
        GridPane gridpane = new GridPane();
        GridPaneObject gridpaneobject = new GridPaneObject(gridpane, 4, 3);
        EchelonController controller = new EchelonController(gridpaneobject);
        ArrayList<Double> matrix3x4 = new ArrayList<>(
            Arrays.asList(1.0, -3.0, 0.0, -3.0, 7.0, 1.0, 3.0, -1.0, -4.0, -2.0, 2.0, 3.0));
        ArrayList<Double> expectedMatrix3x4 = new ArrayList<>(
            Arrays.asList(1.0, 0.0, 0.0, -3.0, 10.0, 0.0, 3.0, -4.0, -3.6, -2.0, 4.0, 2.6));

        gridpaneobject.setGridPaneValues(matrix3x4);
        controller.reduceMatrixToEchelonForm();

        //Testing echelon form
        assertEquals(expectedMatrix3x4, gridpaneobject.getDataAsArrayListDouble()); 

        ArrayList<Double> expectedReducedMatrix3x4 = new ArrayList<>(
            Arrays.asList(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, -9.0, -4.0, 0.0, 0.0, 0.0, 1.0));
        
        controller.reduceMatrixtoReducedEchelonForm();

        //Testing reduced echelon form
        assertEquals(expectedReducedMatrix3x4, gridpaneobject.getDataAsArrayListDouble());
    }

    @Test
    void testReduceMatrix3x3() {
        GridPane gridpane = new GridPane();
        GridPaneObject gridpaneobject = new GridPaneObject(gridpane, 3, 3);
        EchelonController controller = new EchelonController(gridpaneobject);

        ArrayList<Double> matrix3x3 = new ArrayList<>(
            Arrays.asList(1.0, 1.0, -3.0, 3.0, 4.0, -7.0, -5.0, -8.0, 9.0));
        ArrayList<Double> expectedMatrix3x3 = new ArrayList<>(
            Arrays.asList(1.0, 0.0, 0.0, 3.0, 1.0, 0.0, -5.0, -3.0, 0.0));
        
        gridpaneobject.setGridPaneValues(matrix3x3);
        controller.reduceMatrixToEchelonForm();

        //Testing echelon form
        assertEquals(expectedMatrix3x3, gridpaneobject.getDataAsArrayListDouble());

        ArrayList<Double> expectedReducedMatrix3x3 = new ArrayList<>(
            Arrays.asList(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 4.0, -3.0, 0.0));
        
        controller.reduceMatrixtoReducedEchelonForm();

        //Testing reduced echelon form
        assertEquals(expectedReducedMatrix3x3, gridpaneobject.getDataAsArrayListDouble());
    }

    @Test
    void testReduceMatrix2x3() {
        GridPane gridpane = new GridPane();
        GridPaneObject gridpaneobject = new GridPaneObject(gridpane, 3, 2);
        EchelonController controller = new EchelonController(gridpaneobject);

        ArrayList<Double> matrix2x3 = new ArrayList<>(
            Arrays.asList(-5.0, 1.0, 7.0, -2.0, 9.0, 6.0));
        ArrayList<Double> expectedMatrix2x3 = new ArrayList<>(
            Arrays.asList(-5.0, 0.0, 7.0, -0.6, 9.0, 7.8));
        
        gridpaneobject.setGridPaneValues(matrix2x3);
        controller.reduceMatrixToEchelonForm();

        //Testing echelon form
        assertEquals(expectedMatrix2x3, gridpaneobject.getDataAsArrayListDouble());

        ArrayList<Double> expectedReducedMatrix2x3 = new ArrayList<>(
            Arrays.asList(1.0, 0.0, 0.0, 1.0, -20.0, -13.0));
        
        controller.reduceMatrixtoReducedEchelonForm();

        //Testing reduced echelon form
        assertEquals(expectedReducedMatrix2x3, gridpaneobject.getDataAsArrayListDouble());
    }
}
