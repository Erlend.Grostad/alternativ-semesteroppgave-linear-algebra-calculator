module org.openjfx {
    requires transitive javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;

    opens org.openjfx.Calculator.view to javafx.fxml;
    exports org.openjfx.Calculator.view;
}
