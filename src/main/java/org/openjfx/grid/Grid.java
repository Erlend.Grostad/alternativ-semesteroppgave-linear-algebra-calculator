package org.openjfx.grid;

import java.util.ArrayList;

public class Grid implements IGrid {

    private final int rows;
    private final int cols;
    private ArrayList<Double> grid;

    public Grid(int rows, int cols, ArrayList<Double> listOfIntegers) {
        this.rows = rows;
        this.cols = cols;
        this.grid = listOfIntegers;
    }

    @Override
    public int getRows() {
        return this.rows;
    }

    @Override
    public int getCols() {
        return this.cols;
    }

    @Override
    public void set(Coordinate coordinate, Double value) {
        if (coordinateIsOnGrid(coordinate)) {
            grid.set(locationToIndex(coordinate), value);
        }        
    }

    @Override
    public Double get(Coordinate coordinate) {
        return grid.get(locationToIndex(coordinate));        
    }

    @Override
    public boolean coordinateIsOnGrid(Coordinate coordinate) {
        if (indexAllowed(coordinate.row, coordinate.col)) {
            if (locationToIndex(coordinate) >= cols*rows) {
                return false;
            }
            else if (coordinate.row < 0 || coordinate.row >= this.rows) {
                return false;
            }
            else if (coordinate.col < 0 || coordinate.col >= this.cols) {
                return false;
            }
            return true;
        }
        return false;
        
    }

    /**
     * Calculates an index from a Coordinate object
     * @param coordinate
     * @return integer index
     */
    private int locationToIndex(Coordinate coordinate) {
        return coordinate.row + coordinate.col * rows;
    }

    /**
     * Checks a row, column index for negative numbers
     * @return False if either row or column is negative
     */
    private boolean indexAllowed(int row, int col) {
        if (row < 0 || col < 0) {
            return false;
        }
        return true;

    }
    
}
