package org.openjfx.Calculator.view;

import org.openjfx.Calculator.Controller.EchelonController;
import org.openjfx.Calculator.Controller.initialSceneController;
import org.openjfx.Calculator.Model.GridPaneObject;
import org.openjfx.Calculator.Model.sceneBuilder;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class CalculatorView extends Application {

    final private int width = 800;
    final private int height = 600;
    initialSceneController initialController;
    sceneBuilder sceneBuilder;

    public CalculatorView(){
        initialController = new initialSceneController();
        sceneBuilder = new sceneBuilder();
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        try {
            Scene scene = initialScene();
            stage.setTitle("Linear algebra GUI calculator");
            stage.setResizable(false);
            stage.setScene(scene);
            stage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
        
    }

    public Scene initialScene(){
        Group root = new Group();
        Scene scene = new Scene(root, width, height, Color.BLACK);

        //Gridpane represents a mXn or nXn matrix.
        GridPane builtGridPane = sceneBuilder.createGridPane(70, 70, 1.0, 1.0, "Matrix");
        GridPaneObject gridpaneobject = new GridPaneObject(builtGridPane, 3, 3);
        GridPane gridpane = gridpaneobject.getGridpane();

        //Add and remove buttons is used to increase or decrease dimensions of matrix.
        Button buttonAddColumn = sceneBuilder.createButton(150, 20, 1.0, 1.0, "+");
        buttonAddColumn.setOnMouseClicked(new EventHandler<MouseEvent>() {public void handle(MouseEvent e) {initialController.addColumn(gridpaneobject);}});
        Button buttonRemoveColumn = sceneBuilder.createButton(100, 20, 1.0, 1.0, "-");
        buttonRemoveColumn.setOnMouseClicked(new EventHandler<MouseEvent>() {public void handle(MouseEvent e) {initialController.removeColumn(gridpaneobject);}});
        Button buttonAddRow = sceneBuilder.createButton(20, 150, 1.0, 1.0, "+");;
        buttonAddRow.setOnMouseClicked(new EventHandler<MouseEvent>() {public void handle(MouseEvent e) {initialController.addRow(gridpaneobject);}});
        Button buttonRemoveRow = sceneBuilder.createButton(20, 100, 1.0, 1.0, "-");
        buttonRemoveRow.setOnMouseClicked(new EventHandler<MouseEvent>() {public void handle(MouseEvent e) {initialController.removeRow(gridpaneobject);}});

        //Confirm button is used to transition to next scene with the confirmed dimensions, numbers and mode for processing.
        Button confirmButton = sceneBuilder.createButton(400, 500, 3.5, 3.5, "Confirm");
        confirmButton.setOnMouseClicked(new EventHandler<MouseEvent>() {public void handle(MouseEvent e) {
            switchToNewScene(e, echelonScene(gridpaneobject));}});

        //Choicebox is used to represent the mode: e.g. echelon form.
        String[] choices = {"Echelon form", "item2", "item3"};
        ChoiceBox<String> choicebox = sceneBuilder.createChoiceBox(600, 10, 1.0, 1.0, choices);

        root.getChildren().addAll(gridpane, buttonAddColumn, buttonRemoveColumn, buttonAddRow, buttonRemoveRow, choicebox, confirmButton);   
        return scene;
    }

    private Scene echelonScene(GridPaneObject gridpaneobject){
        Group root = new Group();
        Scene scene = new Scene(root, width, height, Color.BLACK);

        // Creates a matrix controller for the echelonScene
        EchelonController echelonController = new EchelonController(gridpaneobject);

        //Creates return button
        Button returnButton = sceneBuilder.createButton(400, 500, 2.0, 2.0, "Return");
        returnButton.setOnMouseClicked(new EventHandler<MouseEvent>() {public void handle(MouseEvent e) {switchToNewScene(e, initialScene());}});

        //Creates autocomplete button for echelonform
        Button echelonFormButton = sceneBuilder.createButton(600, 50, 1.5, 1.5, "EchelonForm");
        echelonFormButton.setOnMouseClicked(new EventHandler<MouseEvent>() {public void handle(MouseEvent e) {
            echelonController.reduceMatrixToEchelonForm();
            switchToNewScene(e, echelonScene(gridpaneobject));
        }});

        // Creates autocomplete button for Reduced echelon form
        Button reducedEchelonFormButton = sceneBuilder.createButton(600, 150, 1.5, 1.5, "Reduced Echelon Form");
        reducedEchelonFormButton.setOnMouseClicked(new EventHandler<MouseEvent>() {public void handle(MouseEvent e) {
            echelonController.reduceMatrixtoReducedEchelonForm();
            switchToNewScene(e, echelonScene(gridpaneobject));
        }});
        
        GridPane gridpane = gridpaneobject.getGridpane();

        root.getChildren().addAll(echelonFormButton, returnButton, gridpane, reducedEchelonFormButton);
        return scene;
    }

    //Helper method for switching scenes
    private void switchToNewScene(MouseEvent event, Scene scene) {
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }
        
}