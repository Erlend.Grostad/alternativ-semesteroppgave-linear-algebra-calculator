package org.openjfx.Calculator.Controller;

public interface IEchelonController {

    /**
     * Reduces a matrix to echelon form
     * @return
     */
    void reduceMatrixToEchelonForm();

    /**
     * reduces a matrix to reduced echelon form
     * @return
     */
    void reduceMatrixtoReducedEchelonForm();

}
