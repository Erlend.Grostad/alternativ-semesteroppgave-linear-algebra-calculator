package org.openjfx.Calculator.Controller;

import org.openjfx.Calculator.Model.GridPaneObject;

public interface IinitialSceneController {

    /**
     * Adds a column to the gridpane in order to represent a m X n+1 matrix.
     * @param gridpane
     */
    void addColumn(GridPaneObject gridpaneobject);

    /**
     * Removes a column from the gridpane in order to represent a m X n-1 matrix.
     * @param gridpane
     */
    void removeColumn(GridPaneObject gridpaneobject);
    
    /**
     * Adds a row to the gridpane in order to represent a m+1 X n matrix.
     * @param gridpane
     */
    void addRow(GridPaneObject gridpaneobject);
    
    /**
     * Removes a row from the gridpane in order to represent a m-1 X n matrix.
     * @param gridpane
     */
    void removeRow(GridPaneObject gridpaneobject);    
}
