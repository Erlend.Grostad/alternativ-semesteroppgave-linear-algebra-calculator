package org.openjfx.Calculator.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.openjfx.Calculator.Model.GridPaneObject;
import org.openjfx.grid.Coordinate;
import org.openjfx.grid.Grid;


public class EchelonController implements IEchelonController {

    Grid matrix;
    GridPaneObject gridpaneobject;
    ArrayList<Coordinate> pivotPositions;
    int rows;
    int cols;

    public EchelonController(GridPaneObject gridpaneobject){
        this.rows = gridpaneobject.getRows();
        this.cols = gridpaneobject.getCols();
        this.gridpaneobject = gridpaneobject;
        this.pivotPositions = new ArrayList<>();
        ArrayList<Double> gridComponent = gridpaneobject.getDataAsArrayListDouble();
        this.matrix = new Grid(rows, cols, gridComponent);
    }

    @Override
    public void reduceMatrixToEchelonForm(){
        ArrayList<Double> doubleList = new ArrayList<>();
        int row = -1;
        int col = -1;
        while (!matrixInEchelonForm()) {
            row++;
            col++;
            Coordinate targetCoordinate = new Coordinate(row, col);
            //If the target coordinate e.g. (coordinate 0,0) is 0, swap rows so number != 0, or searches right for new coordinate
            if (matrix.get(targetCoordinate) == 0) {
                for(int i = targetCoordinate.getRow(); i < rows; i++) {
                    Coordinate newCoordinate = new Coordinate(i, 0);
                    if (matrix.get(newCoordinate) != 0) {
                        shuffleRows(0, i);
                        break;
                    }
                }
                for (int i = targetCoordinate.getCol(); i < cols; i++) {
                    Coordinate newCoordinate = new Coordinate(0, i);
                    if (matrix.get(newCoordinate) != 0) {
                        targetCoordinate = newCoordinate;
                        break;
                    }
                }
            }
            //Make numbers under target coordinate (Coordinate 0,0) = 0
            for (int i = 1; i < rows; i++) {
                int targetRow = targetCoordinate.getRow();
                int targetCol = targetCoordinate.getCol();
                Coordinate AboveCoordinate = new Coordinate(targetRow, targetCol);
                Coordinate BelowCoordinate = new Coordinate(targetRow+i, targetCol);
                if (BelowCoordinate.getRow() >= rows) {
                    break;
                }
                if (matrix.get(BelowCoordinate) == 0) {
                    continue;
                }
                addMultiplumOfRowToAnotherRow(AboveCoordinate, BelowCoordinate, "Echelon");
            }
        }
        setPivotPositions();
        for (int i = 0; i < cols; i++) {
            for (int j = 0; j < rows; j++) {
                Coordinate coordinate = new Coordinate(j, i);
                doubleList.add(matrix.get(coordinate));
            }
        }
        gridpaneobject.setGridPaneValues(doubleList);
    }

    @Override
    public void reduceMatrixtoReducedEchelonForm(){
        if (!matrixInEchelonForm()) {
            reduceMatrixToEchelonForm();
        }
        ArrayList<Double> list = new ArrayList<>();
        int numberOfPivots = this.pivotPositions.size();

        for (int i = numberOfPivots - 1; i > -1; i--) {
            Coordinate coordinate = pivotPositions.get(i);
            for (int j = 1; j < rows; j++) {
                int coordinateRow = coordinate.getRow();
                int coordinateCol = coordinate.getCol();
                Coordinate AboveCoordinate = new Coordinate(coordinateRow, coordinateCol);
                Coordinate BelowCoordinate = new Coordinate(coordinateRow-j, coordinateCol);
                if (BelowCoordinate.getRow() < 0) {
                    break;
                }
                if (matrix.get(BelowCoordinate) == 0) {
                    continue;
                }
                addMultiplumOfRowToAnotherRow(AboveCoordinate, BelowCoordinate, "ReducedEchelon");
            }
        }
        if (!allPivotsAreEqualOne()){
            for (int i = 0; i < numberOfPivots; i++) {
                Double pivotValue = matrix.get(pivotPositions.get(i));
                if (pivotValue != 1) {
                    multiplyRowWithNumberNotZero(pivotPositions.get(i));
                }
            }
        }
        if (matrixInReducedEchelonForm()) {
            for (int i = 0; i < cols; i++) {
                for (int j = 0; j < rows; j++) {
                    Coordinate coordinate = new Coordinate(j, i);
                    list.add(matrix.get(coordinate));
                }
            }
            gridpaneobject.setGridPaneValues(list);
        }
        else {
            System.out.println("Reduced Echelon Form calculation error");
        }
 
    }

    private boolean matrixInEchelonForm() {
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                Coordinate checkCoordinate = new Coordinate(row, col);
                if (matrix.get(checkCoordinate) != 0) {
                    if (!columnIsInEchelonForm(checkCoordinate)) {
                        return false;
                    }
                    else {
                        break;
                    }
                }
            }
        }
        return true;
    }

    private boolean matrixInReducedEchelonForm() {
        if (!matrixInEchelonForm()) {
            return false;
        }
        for (int i = 0; i < pivotPositions.size(); i++) {
            if (!columnIsInReducedEchelonForm(pivotPositions.get(i))) {
                return false;
            }
        }
        return true;
    }

    private boolean allPivotsAreEqualOne() {
        int size = pivotPositions.size();
        for (int i = 0; i < size; i++) {
            Double pivotValue = matrix.get(pivotPositions.get(i));
            if (pivotValue != 1) {
                return false;
            }            
        }
        return true;
    }

    // A column is in echelon form if every number left and under the pivot is 0.
    private boolean columnIsInEchelonForm(Coordinate coordinate) {
        int coordinateRow = coordinate.getRow();
        int coordinateCol = coordinate.getCol();
        // Check values left of coordinate
        for (int row = coordinateRow; row < rows; row++) {
            for (int col = coordinateCol - 1; col > -1; col--) {
                if (col < 0) {
                    break;
                }
                Coordinate checkCoordinateLeft = new Coordinate(row, col);
                if (matrix.get(checkCoordinateLeft) != 0) {
                    return false;
                }
            }            
        }
        // Check values under coordinate
        for (int row = coordinateRow + 1; row < rows; row++) {
            if (row >= rows) {
                break;
            }
            Coordinate checkCoordinateLeft = new Coordinate(row, coordinateCol);
            if (matrix.get(checkCoordinateLeft) != 0) {
                return false;
            }
        }            
        return true;
    }

    private boolean columnIsInReducedEchelonForm(Coordinate coordinate) {
        int coordinateRow = coordinate.getRow();
        int coordinateCol = coordinate.getCol();
        for (int row = coordinateRow - 1; row > -1; row--) {
            if (row <= -1) {
                break;
            }
            Coordinate checkCoordinateUp = new Coordinate(row, coordinateCol);
            if (matrix.get(checkCoordinateUp) != 0) {
                return false;
            }
        }
        return true;
    }

    //
    private void addMultiplumOfRowToAnotherRow(Coordinate coordinateSource, Coordinate coordinateTarget, String mode) {
        //Finds the number needed to multiply value at original coordinate in order to make a chosen number below = 0. (-under/above)
        Double a = matrix.get(coordinateSource);
        Double b = matrix.get(coordinateTarget);
        Double multiplum = -b/a;
        // Iterates along row multiplying the row under by the row over according to the multiplum.
        if (mode == "Echelon") {
            for (int i = 0; i < cols; i++) {
                Coordinate row1 = new Coordinate(coordinateSource.getRow(), i);
                Coordinate row2 = new Coordinate(coordinateTarget.getRow(), i);
                matrix.set(row2, (matrix.get(row1) * multiplum + matrix.get(row2)));
            }
        }
        else if (mode == "ReducedEchelon") {
            for (int i = 0; i < cols; i++) {
                Coordinate row1 = new Coordinate(coordinateSource.getRow(), i);
                Coordinate row2 = new Coordinate(coordinateTarget.getRow(), i);
                matrix.set(row2, (matrix.get(row1) * multiplum + matrix.get(row2)));
            }
        }
        
    }

    private void multiplyRowWithNumberNotZero(Coordinate coordinate) {
        Double value = matrix.get(coordinate);
        int coordRow = coordinate.getRow();
        Double multiplum = 1/value;
        for (int i = 0; i < cols; i++) {
            Coordinate newCoordinate = new Coordinate(coordRow, i);
            matrix.set(newCoordinate, matrix.get(newCoordinate) * multiplum);
        }
    }

    private void shuffleRows(int targetRow, int replacingRow) {
        for (int i = 0; i < cols; i++) {
            Coordinate targetCoordinate = new Coordinate(targetRow, i);
            Coordinate replacingCoordinate = new Coordinate(replacingRow, i);
            Double targetValue = matrix.get(targetCoordinate);
            Double replacingValue = matrix.get(replacingCoordinate);
            matrix.set(targetCoordinate, replacingValue);
            matrix.set(replacingCoordinate, targetValue);

        }
    }    

    private void setPivotPositions(){
        int row = 0;
        int col = 0;
        while (row < rows && col < cols) {
            Coordinate targetCoordinate = new Coordinate(row, col);
            if (matrix.get(targetCoordinate) == 0) {
                for (int i = 1; i < cols; i++) {
                    if (targetCoordinate.getCol() + i < cols) {
                        Coordinate newCoordinate = new Coordinate(targetCoordinate.getRow(), targetCoordinate.getCol() + i);
                        if (matrix.get(newCoordinate) != 0) {
                            pivotPositions.add(newCoordinate);
                            break;
                        }
                    }
                    else {
                        break;
                    }                    
                }
            }
            else {
                pivotPositions.add(targetCoordinate);
            }
            row++;
            col++;
        }        
    }
}
