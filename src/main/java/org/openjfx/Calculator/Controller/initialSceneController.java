package org.openjfx.Calculator.Controller;

import java.util.ArrayList;
import java.util.List;

import org.openjfx.Calculator.Model.GridPaneObject;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class initialSceneController implements IinitialSceneController{

    //Sets upper and lower limit for rows and columns in Matrix.
    final private int UPPER_LIMIT = 10;
    final private int LOWER_LIMIT = 2;


    @Override
    public void addColumn(GridPaneObject gridpaneobject) {
        GridPane gridpane = gridpaneobject.getGridpane();
        int cols = gridpane.getColumnCount();
        if (cols > UPPER_LIMIT) {
            return;
        }
        int rows = gridpane.getRowCount();
        for (int i = 0; i < rows; i++) {
            TextField text = new TextField("0");
            text.setPrefColumnCount(3);
            gridpane.add(text, cols, i);
        }
        gridpaneobject.setCols(cols+1);
    }

    @Override
    public void removeColumn(GridPaneObject gridpaneobject) {
        GridPane gridpane = gridpaneobject.getGridpane();
        int cols = gridpane.getColumnCount();
        if (cols == LOWER_LIMIT) {
            return;
        }
        ObservableList<Node> children = gridpane.getChildren();
        List<Node> temp = new ArrayList<>();
        for (Node child : children) {
            if (GridPane.getColumnIndex(child) == null) {
                temp.add(child);
            }
            else if (GridPane.getColumnIndex(child) == cols-1) {
                    temp.add(child);
                }
        }
        gridpane.getChildren().removeAll(temp);
        gridpaneobject.setCols(cols-1);
    }

    @Override
    public void addRow(GridPaneObject gridpaneobject) {
        GridPane gridpane = gridpaneobject.getGridpane();
        int cols = gridpane.getColumnCount();
        int rows = gridpane.getRowCount();
        if (rows > UPPER_LIMIT) {
            return;
        }
        for (int i = 0; i < cols; i++) {
            TextField text = new TextField("0");
            text.setPrefColumnCount(3);
            gridpane.add(text, i, rows);
        }
        gridpaneobject.setRows(rows+1);
    }

    @Override
    public void removeRow(GridPaneObject gridpaneobject) {
        GridPane gridpane = gridpaneobject.getGridpane();
        int rows = gridpane.getRowCount();
        if (rows == LOWER_LIMIT) {
            return;
        }
        ObservableList<Node> children = gridpane.getChildren();
        List<Node> temp = new ArrayList<>();
        for (Node child : children) {
            if (GridPane.getRowIndex(child) == null) {
                temp.add(child);
            }
            else if (GridPane.getRowIndex(child) == rows-1) {
                    temp.add(child);
                }
        }
        gridpane.getChildren().removeAll(temp);
        gridpaneobject.setRows(rows-1);
    }
}