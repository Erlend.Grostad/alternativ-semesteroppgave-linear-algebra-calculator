package org.openjfx.Calculator.Model;

import java.util.ArrayList;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class GridPaneObject extends sceneBuilder {
    
    private GridPane gridpane;
    private int cols;
    private int rows;


    public GridPaneObject(GridPane gridpane, int cols, int rows) {
        this.gridpane = gridpane;
        this.cols = cols;
        this.rows = rows;
        initializeInitialPane(this.cols, this.rows, this.gridpane);
    }


    public GridPane getGridpane() {
        return this.gridpane;
    }

    public void setGridpane(GridPane gridpane) {
        this.gridpane = gridpane;
    }

    public int getCols() {
        return this.cols;
    }

    public void setCols(int cols) {
        this.cols = cols;
    }

    public int getRows() {
        return this.rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public ArrayList<Double> getDataAsArrayListDouble(){
        ArrayList<Double> stringList = new ArrayList<>();
        ObservableList<Node> list = gridpane.getChildren();
        for (int i = 0; i < list.size(); i++) {
            TextField text = (TextField) list.get(i);
            Double number = Double.parseDouble(text.getCharacters().toString());
            stringList.add(number);
        }
        return stringList;
    }

    public void setGridPaneValues(ArrayList<Double> list) {
        gridpane.getChildren().removeAll(gridpane.getChildren());
        int index = 0;
        for (int i = 0; i < cols; i++){
            for (int j = 0; j < rows; j++){
                String text = list.get(index).toString();
                TextField textfield = new TextField(text);
                textfield.setPrefColumnCount(3);
                gridpane.add(textfield, i, j);
                index++;
            }
        }
    }

    private void initializeInitialPane(int a, int b, GridPane gridpane){
        for (int i = 0; i < a; i++){
            for (int j = 0; j < b; j++){
                TextField text = new TextField("0");
                text.setPrefColumnCount(3);
                gridpane.add(text, i, j);
            }
        }
    }



}
