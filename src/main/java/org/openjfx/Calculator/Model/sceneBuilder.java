package org.openjfx.Calculator.Model;

import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.GridPane;

public class sceneBuilder implements IsceneBuilder {

    @Override
    public Button createButton(int layoutX, int layoutY, double scaleX, double scaleY, String label) {
        Button button = new Button(label);
        button.setLayoutX(layoutX);
        button.setLayoutY(layoutY);
        button.setScaleX(scaleX);
        button.setScaleY(scaleY);
        return button;
    }

    @Override
    public GridPane createGridPane(int layoutX, int layoutY, double scaleX, double scaleY, String id) {
        GridPane gridpane = new GridPane();
        gridpane.setLayoutX(layoutX);
        gridpane.setLayoutY(layoutY);
        gridpane.setScaleX(scaleX);
        gridpane.setScaleY(scaleY);
        gridpane.setId(id);
        return gridpane;
    }
/*
    @Override
    public void editGridPane(GridPane gridpane, Double hgap, Double vgap, boolean editableBool) {
        gridpane.setHgap(hgap);
        gridpane.setVgap(vgap);
        ObservableList<Node> children = gridpane.getChildren();        
        for (Node node : children) {
            TextField text = (TextField) node;
            text.setEditable(editableBool);
        }
    }
    */

    @Override
    public ChoiceBox<String> createChoiceBox(int layoutX, int layoutY, double scaleX, double scaleY, String[] choices) {
        ChoiceBox<String> choicebox = new ChoiceBox<>();
        choicebox.setLayoutX(layoutX);
        choicebox.setLayoutY(layoutY);
        choicebox.setScaleX(scaleX);
        choicebox.setScaleY(scaleY);
        for (String choice : choices) {
            choicebox.getItems().add(choice);
        }
        return choicebox;
    }
    
}
