package org.openjfx.Calculator.Model;

import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.control.ChoiceBox;

public interface IsceneBuilder {

    /**
     * @param layoutX
     * @param layoutY
     * @param scaleX
     * @param scaleY
     * @param label - Text that appears on button
     * @return a button object to set as node in a scene
     */
    Button createButton(int layoutX, int layoutY, double scaleX, double scaleY, String label);

    /**
     * 
     * @param layoutX
     * @param layoutY
     * @param scaleX
     * @param scaleY
     * @param id - unique identifier for use in transferring data when switching between scenes
     * @param cols 
     * @param rows
     * @return a gridpane object to set as node in a scene
     */
    GridPane createGridPane(int layoutX, int layoutY, double scaleX, double scaleY, String id);

    /**
     * @param layoutX
     * @param layoutY
     * @param scaleX
     * @param scaleY
     * @param choices a list of choices for the choicebox
     * @return a choicebox to set as node in a scene
     */
    ChoiceBox<String> createChoiceBox(int layoutX, int layoutY, double scaleX, double scaleY, String[] choices);
}
